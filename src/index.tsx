import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { PlaceViewer } from './components/PlaceViewer';

ReactDOM.render(
	<PlaceViewer />,
	document.getElementById('app')
);
